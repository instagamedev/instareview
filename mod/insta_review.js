/*
instaReview v0.2
mod for GameDev Tycoon
author: 0Ds0
Description: makes review scores show up instantly
*/

// Enable copy paste into node-webkit
// var gui = require('nw.gui');
// win = gui.Window.get();
// var nativeMenuBar = new gui.Menu({ type: "menubar" });
// try {
// nativeMenuBar.createMacBuiltin("My App");
// win.menu = nativeMenuBar;
// } catch (ex) {
// console.log(ex.message);
// }


(function(){
    var originalShowReviewWindow = UI.showReviewWindow;

    var instaReview = function(b, c){
        var d = $("#reviewWindow");
        d.find("#reviewAnimationContainer").empty();
        d.find(".okButton").hide().clickExclOnce(function() {
            g = [];
            k = [];
            UI.closeModal(c)
        });
        UI.showModalContent("#reviewWindow", {
            disableCheckForNotifications: !0,
            onOpen: function() {
                instaOpen();
            },
            onClose: function() {
                GameManager.company.activeNotifications.remove(b)
            }
        })

    };

    UI.showReviewWindow = instaReview;

    var instaOpen = function() {
        // console.log('--instaOpen--');
        console.log(process.versions['node-webkit']);
        var $simple_modal = $(".simplemodal-data"),
            $reviewAnimation = $simple_modal.find("#reviewAnimationContainer"),
            $reviews_template = $("#reviewItemTemplate"),
            gm = GameManager.company.gameLog.last(),
            modal_title = "Reviews for {0}".localize().format(gm.title),
            $window_title = $simple_modal.find(".windowTitle").text(modal_title);

            gm.reviews.forEach(function(element, index, array){
                $cloneReview = $reviews_template.clone();
                $cloneReview.removeAttr("id");
                $cloneReview.scoreElement = $cloneReview.find(".score").text(element.score);
                $cloneReview.textElement = $cloneReview.find(".text").text(element.message).css('min-width', 160);
                $cloneReview.reviewerElement = $cloneReview.find(".reviewer").text('... ' + element.reviewerName);
                $cloneReview.award = $cloneReview.find(".award").css('opacity', (element.score >= 10)? 1 : 0 );

                $stars = $cloneReview.find('.stars');
                $stars.find('.star').css('display', 'none');

                if (element.score % 2 === 0) {
                    var $s = $stars.find('.star.full');
                    $res = $s.slice(0, Math.floor(element.score/2));
                    $res.css('display', 'inline');
                } else {
                    if(element.score == 1) {
                        $stars.find('.star.half:first').css('display', 'inline-block');
                    } else {
                        var $s = $stars.find('.star.full');
                        $res = $s.slice(0, Math.floor(element.score/2));
                        $res.css('display', 'inline-block');
                        setTimeout(function() {
                            $stars.find('.star.full:visible:last').next('.star.half').css('display', 'inline-block');
                        }, 0);
                    }
                }

                $cloneReview.css('margin-left', 120);
                if (index !== 0) {
                    $cloneReview.css('margin-top', 120 * index);
                }
                $reviewAnimation.append($cloneReview)
            });

            $simple_modal.find(".okButton").css('margin', '0 auto').slideDown();
    };


})();